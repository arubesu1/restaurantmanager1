package com.arubesu.manager;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.arubesu.manager.config.SecurityConfig;
import com.arubesu.manager.controller.RestaurantController;
import com.arubesu.manager.repository.RestaurantRepository;

@RunWith(SpringRunner.class)
@Import(SecurityConfig.class)

@WebMvcTest(RestaurantController.class)



@WithMockUser
public class RestaurantControllerAuthFuncionarioTest {

	@Autowired
	private WebApplicationContext context;
	
	@Autowired
	private MockMvc mvc;
	

	
	@MockBean
	private RestaurantRepository restaurantRepository;
	
	@Before
	public void setup() {
		
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity()) 
				.build();
	}

	
	

	@Test
	public void testAdd() throws Exception {
		
		this.mvc.perform(post("/restaurants")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is4xxClientError());
				
	}
	
	@Test
	public void testListAll() throws Exception {
		this.mvc.perform(get("/restaurants")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	

	@Test
	public void testRetrieveRestaurant() throws Exception {
		this.mvc.perform(get("/restaurants/RestauranteTest")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	@Test
	public void testDelete() throws Exception {
		this.mvc.perform(delete("/restaurants/10")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is4xxClientError());
	}

}
