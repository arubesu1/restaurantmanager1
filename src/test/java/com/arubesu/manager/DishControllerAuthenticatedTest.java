package com.arubesu.manager;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


import com.arubesu.manager.controller.DishController;
import com.arubesu.manager.model.Dish;
import com.arubesu.manager.repository.DishRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@ContextConfiguration
@WebMvcTest(DishController.class)
@WithMockUser(username = "user" , roles = {"USER", "ADMIN"})
public class DishControllerAuthenticatedTest {
	
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private ObjectMapper mapper;
	
	private Dish dish;
	
	@Before
	public void setup() {
		dish = new Dish(10L , "RestauranteTest", "Lasanha", 20);
	}
	
	@MockBean
	private DishRepository dishRepository;
	
	

	@Test
	public void testAdd() throws Exception {
		String json = mapper.writeValueAsString(dish);
		this.mvc.perform(post("/dishes")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	public void testListAll() throws Exception {
		this.mvc.perform(get("/dishes")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	

	@Test
	public void testRetrieveDish() throws Exception {
		this.mvc.perform(get("/dishes/Lasanha")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	@Test
	public void testDelete() throws Exception {
		this.mvc.perform(delete("/dishes/10")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

}
