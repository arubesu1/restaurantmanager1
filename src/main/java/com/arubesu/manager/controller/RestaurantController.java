package com.arubesu.manager.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.arubesu.manager.model.Restaurant;
import com.arubesu.manager.repository.RestaurantRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")

public class RestaurantController {
	
	@Autowired
	private RestaurantRepository restaurantRepository;
	
	//Método responsável por retornar todos os restaurantes do banco de dados 

	@GetMapping("/restaurants")
	@PreAuthorize("hasRole('USER')")
	public List<Restaurant> listAll(){
		return restaurantRepository.findAll();
	}

	//Método responsável por cadastrar o restaurante no banco de dados 

	@PostMapping("/restaurants")
	@PreAuthorize("hasRole('ADMIN')")
	public Restaurant add(@RequestBody @Valid Restaurant restaurant) {
		
		return restaurantRepository.save(restaurant);
		
		
	}	
	
	//Método responsável por deletar determinado restaurante pelo id , no banco de dados 

	@DeleteMapping("/restaurants/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public void delete(@PathVariable Long id) {
		restaurantRepository.delete(id);
	}
	
	//Método responsável por editar determinado restaurante pelo id , no banco de dados 

	@PutMapping("/restaurants/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Object> update(@RequestBody Restaurant restaurant, @PathVariable Long id){
		
		Restaurant restaurantTmp = restaurantRepository.findOne(id);
		
		if(restaurantTmp.equals(null)) {
			return ResponseEntity.notFound().build();
		}
		
		restaurant.setId(id);
		restaurantRepository.save(restaurant);
		
		return ResponseEntity.ok().build();
		
	}
	
	//Método responsável por buscar determinado restaurante pelo nome no banco de dados 

	@GetMapping("restaurants/{name}")
	@PreAuthorize("hasRole('USER')")
	public List<Restaurant> retrieveRestaurant(@PathVariable String name ) {
		
		
		return restaurantRepository.findByName(name);
	}
	
}
