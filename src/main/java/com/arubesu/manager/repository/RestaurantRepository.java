package com.arubesu.manager.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.arubesu.manager.model.Restaurant;

public interface RestaurantRepository extends JpaRepository<Restaurant, Long> {
	
	@Query(value = "SELECT * FROM restaurant WHERE name = ?1", nativeQuery = true)
	public List<Restaurant> findByName(String name);

}
