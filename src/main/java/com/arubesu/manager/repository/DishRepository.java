package com.arubesu.manager.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.arubesu.manager.model.Dish;

public interface DishRepository extends JpaRepository<Dish, Long> {
	
	@Query(value = "SELECT * FROM dish WHERE name = ?1", nativeQuery = true)
	public List<Dish> findByName(String name);

}
