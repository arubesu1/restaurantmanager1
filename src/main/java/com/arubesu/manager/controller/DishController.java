package com.arubesu.manager.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.arubesu.manager.model.Dish;
import com.arubesu.manager.repository.DishRepository;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class DishController {
	
	@Autowired
	private DishRepository dishRepository;
	
	//Método responsável por retornar todos os pratos do banco de dados 
	@GetMapping("/dishes")
	@PreAuthorize("hasRole('USER')")
	public List<Dish> listAll(){
		return dishRepository.findAll();
	}
	
	//Método responsável por cadastrar o prato no banco de dados 
	@PostMapping("/dishes")
	@PreAuthorize("hasRole('USER')")
	public Dish add(@RequestBody @Valid Dish dish) {
		return dishRepository.save(dish);
	}
	
	//Método responsável por deletar determinado prato pelo id , no banco de dados 
	@DeleteMapping("/dishes/{id}")
	@PreAuthorize("hasRole('USER')")
	public void delete(@PathVariable Long id) {
		dishRepository.delete(id);
	}
	
	//Método responsável por editar determinado prato pelo id , no banco de dados 
	@PutMapping("/dishes/{id}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Object> update(@RequestBody Dish dish, @PathVariable Long id){
		
		Dish dishTmp = dishRepository.findOne(id);
		
		if(dishTmp.equals(null)) {
			return ResponseEntity.notFound().build();
		}
		
		dish.setId(id);
		dishRepository.save(dish);
		
		return ResponseEntity.ok().build();
		
	}
	
	//Método responsável por buscar determinado prato pelo nome no banco de dados 

	@GetMapping("dishes/{name}")
	@PreAuthorize("hasRole('USER')")
	public List<Dish> retrieveDish(@PathVariable String name ) {
		
		
		return dishRepository.findByName(name);
	}
	
	
	
	
	
	
	

}
